import React, { Component } from 'react';
import { Fragment } from 'react';
class AppHeader extends Component {

    render() {
        return (
            <Fragment>
                <h1>
                    This is Header with props {this.props.header}
                </h1>
            </Fragment>
        );
    }
}

export default AppHeader;