import React, { Component } from 'react';
import "bootstrap/dist/css/bootstrap.css"
import ContactList from './ContactList';
class Card extends Component {

    state = {contacts:[]};

    render() {
        return (
            
            <div className='container'>
                <p>{this.state.contacts.length} contacts exist.</p>
                <div className='row'>
                    <div className='col'>
                        <ContactList contacts={this.state.contacts}/>
                    </div>
                    <div className='col'></div>
                </div>

            </div>
        );
    }
    componentDidMount() {
        fetch('http://localhost:4000/contacts')
        .then(response=> response.json())
        .then(data=>this.setState({contacts:data}));
        }
}



export default Card;