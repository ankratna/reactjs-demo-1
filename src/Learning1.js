import React, { Component } from 'react';
// import MyForm from "./MyForm";
import AppContent from "./AppContent";
class Learning1 extends Component {

    render() {
        const msg = "Hello world";
        const name = "Ankit"
        return (
            <div>
            <hr/>
            <h1>{msg} from {name}</h1>
            <hr/>
            <AppContent/>
            <hr/>
            {/* <MyForm/> */}
            </div>
        );
    }
}

export default Learning1;