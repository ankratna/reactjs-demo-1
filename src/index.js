import React,{Component} from "react";
import  ReactDOM  from "react-dom";
import AppFooter from "./AppFooter";
import AppHeader from "./AppHeader";
import Card from "./Card";
class App extends Component{
render(){

    const footer = {
        name : "footer",
        year: 2023,
    }
    return(<div>
        <AppHeader header = "header props"/>
        <hr/>
        <Card/>
        <hr/>
        <AppFooter {...footer}/>
        </div>
        );
}
}

// ReactDOM.render(<App/>, document.getElementById('root');)
ReactDOM.render(<App/>,document.getElementById('root'));


