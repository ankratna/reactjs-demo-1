import React, { Component } from "react";

const Btn = (props) => (
  <button onClick={props.handler} className="btn btn-primary">
    {props.caption}
  </button>
);

class AppContent extends Component {
  // constructor(props) {
  //     super(props);

  // }

  state = {
    num: 0,
    name: "",
    formErrors: {
      name: "Name is required",
    },
    erroMessage: null,
  };

  buttonClick = () => {
    console.log("Primary button is clicked");
  };

  incrementOrDecrement = (incVal) => {
    this.setState(this.setState({ num: this.state.num + incVal }));
  };

  validateForm = (formErrors) => {
    let valid = true;
    Object.values(formErrors).forEach(
      (err) => (valid = valid && err.length === 0)
    );
    return valid;
  };

  onClickHandler = (evt) => {
    evt.preventDefault();
    //@Todo validate form is pending and error message below a button
    let { formErrors } = this.state;
    if (this.validateForm(this.state.formErrors)) {
      alert("All is well");
    }
    let erroMessage = Object.values(formErrors).map((err, index) =>
      err.length === 0 ? null : <li key={index}>{err}</li>
    );
    this.setState({ erroMessage });
  };

  tfHandler = (event) => {
    let { name, value } = event.target;
    let { formErrors } = this.state;
    console.log(name, value);
    console.log(formErrors);

    switch (name) {
      case "name":
        if (!value || value.length === 0) {
          formErrors.name = "Name is required";
        } else if (value.length < 3 || value.length > 20) {
          formErrors.name = "Name should be in between 3 and 20";
        } else {
          formErrors.name = "";
        }
        break;
      default:
        break;
    }
    // const name = event.target.name;
    // const value = event.target.value;
    this.setState({ name: value, formErrors });
    // this.setState({name:value});
  };

  render() {
    return (
      <div>
        <h1>This is App Content Component.</h1>
        <h2>{JSON.stringify(this.state)}</h2>
        <hr />
        <button onClick={this.buttonClick} className="btn btn-primary">
          {" "}
          ClickMe{" "}
        </button>
        <hr />
        <h2>Value of counter is {this.state.num}</h2>
        {/* <button className='btn btn-primary' onClick={()=>{this.incrementOrDecrement(1)}}>increment</button>
                <button className='btn btn-primary' onClick={()=>{this.incrementOrDecrement(-1)}}>decrement</button> */}
        <hr />
        <Btn caption="inc" handler={() => this.incrementOrDecrement(1)}></Btn>
        <Btn caption="dec" handler={() => this.incrementOrDecrement(-1)}></Btn>
        <hr />
        <form className="form">
          <div className="form-group row">
            <label className="control-label col-md-4">Name</label>
            <div className="col-md-8">
              <input
                onChange={this.tfHandler}
                name="name"
                type="text"
                className="form-control"
              />
            </div>
          </div>
          <button
            type="button"
            onClick={this.onClickHandler}
            className="btn btn-primary"
          >
            Submit
          </button>
          <hr></hr>
          <ul>{this.state.erroMessage}</ul>
        </form>
      </div>
    );
  }
}

export default AppContent;
