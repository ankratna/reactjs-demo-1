function AppFooter(props) {
    return(
        <div>
            This is App Footer. with props {props.name} and year {props.year}
        </div>
    );
}

export default AppFooter;